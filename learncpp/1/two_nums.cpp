#include <iostream>

int main()
{
    std::cout << "Enter a number: ";
    int x;
    std::cin >> x;
    
    std::cout << "Enter another number: ";
    int y;
    std::cin >> y;

    std::cout << x << "+" << y << "=" << x + y << std::endl;
    std::cout << x << "-" << y << "=" << x - y << std::endl;
    
    return 0;
}
