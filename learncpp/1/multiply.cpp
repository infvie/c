#include <iostream>

int main()
{
    std::cout << "Enter a number: ";
    
    int x;

    std::cin >> x;

    std::cout << "Double the number is " << 2*x << std::endl;
    std::cout << "Triple the number is " << 3*x << std::endl;

    return 0;
}
    

