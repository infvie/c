#include <iostream>

int readNumber()
{
    std::cout << "Enter a number: ";
    int x;
    std::cin >> x;
    return x;
}

void writeNumber(int x)
{
    std::cout << "The answer the is " << x << std::endl;
}

int main()
{ 
    int x,y;
    x = readNumber();
    y = readNumber();
    writeNumber(x+y);
    return 0;
}
